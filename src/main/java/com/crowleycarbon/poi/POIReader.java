/**
 =====================
 The MIT License (MIT)
 =====================

 Copyright (c) 2020 Crowley Carbon Ltd

 Permission is hereby granted, free of charge,
 to any person obtaining a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

//
// History:
//  Mar 2020 John MacEnri Creation
//

package com.crowleycarbon.poi;

import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class POIReader
{
    private Workbook workbook;
    private DataFormatter formatter;
    private FormulaEvaluator evaluator;
    private String separator;
    private int escapeStyle;
    private boolean formattedCells;

    private CellStyle dateCellStyle;
    private CellStyle dateTimeCellStyle;


    public static final String DEFAULT_SEPARATOR = ",";

    /**
     * Identifies that the CSV file should obey Excel's formatting conventions
     * with regard to escaping certain embedded characters - the field separator,
     * speech mark and end of line (EOL) character
     */
    public static final int EXCEL_STYLE_ESCAPING = 0;

    /**
     * Signals that numeric cells with a formatter indicating it's a date cell
     * should be rendered in the CSV according to that format.
     */
    public static final boolean CELLS_FORMATTED = false;

    /**
     * Identifies that the CSV file should obey UNIX formatting conventions
     * with regard to escaping certain embedded characters - the field separator
     * and end of line (EOL) character
     */
    public static final int UNIX_STYLE_ESCAPING = 1;

    public static final String DEFAULT_DATE_FORMAT = "YYYY-MM-DD";
    public static final String DEFAULT_DATETIME_FORMAT = "YYYY-MM-DD hh:mm:ss";

    public POIReader() throws IOException
    {
        this(DEFAULT_SEPARATOR, EXCEL_STYLE_ESCAPING, CELLS_FORMATTED);
    }

    public POIReader(String sep, int esc, boolean formattedCells) throws IOException
    {
        this((InputStream)null, DEFAULT_SEPARATOR, EXCEL_STYLE_ESCAPING, CELLS_FORMATTED);
    }

    public POIReader(String filename) throws IOException
    {
        this(filename, DEFAULT_SEPARATOR, EXCEL_STYLE_ESCAPING, CELLS_FORMATTED);
    }

    public POIReader(String filename, String sep, int esc, boolean formattedCells) throws IOException
    {
        this(new FileInputStream(new File(filename)), sep, esc, formattedCells);
    }

    public POIReader(byte[] buf, String sep, int esc, boolean formattedCells) throws IOException
    {
        this(new ByteArrayInputStream(buf), sep, esc, formattedCells);
    }

    public POIReader(InputStream is, String sep, int esc, boolean formattedCells) throws IOException
    {
        if (is == null) this.workbook = WorkbookFactory.create(true);
        else this.workbook = WorkbookFactory.create(is);
        CreationHelper createHelper = this.workbook.getCreationHelper();

        this.evaluator = this.workbook.getCreationHelper().createFormulaEvaluator();
        this.formatter = new DataFormatter(true);
        this.separator = sep;
        this.escapeStyle = esc;
        this.formattedCells = formattedCells;
        this.dateCellStyle = this.workbook.createCellStyle();
        this.dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat(DEFAULT_DATE_FORMAT));
        this.dateTimeCellStyle = this.workbook.createCellStyle();
        this.dateTimeCellStyle.setDataFormat(createHelper.createDataFormat().getFormat(DEFAULT_DATETIME_FORMAT));
    }

    public POIReader setDateFormat(String dateFormat)
    {
        this.dateCellStyle.setDataFormat(this.workbook.getCreationHelper().createDataFormat().getFormat(dateFormat));
        return this;
    }

    public POIReader setDateTimeFormat(String dateTimeFormat)
    {
        this.dateTimeCellStyle.setDataFormat(this.workbook.getCreationHelper().createDataFormat().getFormat(dateTimeFormat));
        return this;
    }

    public String[] getSheetNames()
    {
        int numSheets = workbook.getNumberOfSheets();
        String[] names = new String[numSheets];
        for (int i = 0; i < numSheets; i++) {
            names[i] = workbook.getSheetName(i);
        }
        return names;
    }

    public String getSheetIdAsCsv(int sheetIndex)
    {
        return getSheetNameAsCsv(getSheetNames()[sheetIndex]);
    }

    public String getSheetNameAsCsv(String sheetName)
    {
        List<List<String>> csvData = new ArrayList<>();
        Sheet sheet = workbook.getSheet(sheetName);
        if (sheet == null) throw new RuntimeException("No sheet called " + sheetName + " found in file ");
        if (sheet.getPhysicalNumberOfRows() == 0)
        {
            return "";
        }

        int numRows = sheet.getLastRowNum();
        int maxRowWidth = 0;
        for (int i = 0; i <= numRows; i++)
        {
            Row row = sheet.getRow(i);
            List<String> rowCsv = rowToList(row);
            csvData.add(rowCsv);
            if (rowCsv.size() > maxRowWidth)
            {
                maxRowWidth = rowCsv.size();
            }
        }
        return csvDataToString(csvData, maxRowWidth);
    }

    public int writeSheetData(String sheetName, List<List<Object>> data, boolean clearSheet)
    {
        return writeSheetData(sheetName, data, clearSheet, -1, 1);
    }

    public int writeSheetData(String sheetName, List<List<Object>> data, boolean clearSheet, int rowLocation, int colLocation)
    {
        //System.out.println("rowLocation is " + rowLocation);
        //System.out.println("colLocation is " + colLocation);
        if (rowLocation < 0) rowLocation = 0;  //Anything < 0, treat as append (0)
        if (colLocation < 1) colLocation = 1;  //Anything < 1, treat as first col (1)

        boolean sheetExists = Arrays.asList(getSheetNames()).contains(sheetName);
        if (!sheetExists)
        {
            this.workbook.createSheet(sheetName);
            //System.out.println("Sheet Created.");
        }
        Sheet sheet = sheet = workbook.getSheet(sheetName);
        //System.out.println("1. LastRowNum is " + sheet.getLastRowNum());

        //Clear existing data after specified rowLocation
        if (clearSheet)
        {
            int clearFrom = rowLocation - 1;
            if (clearFrom <= 0) clearFrom = 0;
            int clearTo = sheet.getLastRowNum();
            //System.out.println("Remove rows from " + clearFrom + " to " + clearTo);

            for (int rowId = clearFrom; rowId <= clearTo; rowId++)
            {
                Row row = sheet.getRow(rowId);
                if (row != null)
                {
                    sheet.removeRow(row);
                    //System.out.println("Removed row " + rowId);
                }
//                else
//                {
//                    System.out.println("Could not find row at rowNum " + rowId);
//                }
            }
        }
        //System.out.println("2. LastRowNum is " + sheet.getLastRowNum());

        //Create empty rows up to rowLocation if required
        //System.out.println("Insert rows from " + (sheet.getLastRowNum() + 1) + " to " + rowLocation);
        for (int rowId = sheet.getLastRowNum() + 1; rowId < rowLocation; rowId++)
        {
            sheet.createRow(rowId);
        }

        //System.out.println("3. LastRowNum is " + sheet.getLastRowNum());

        int startRow = rowLocation - 1;
        if (rowLocation == 0)  //Requested to append to existing rows
        {
            if (sheet.getLastRowNum() > 0) startRow = sheet.getLastRowNum() + 1;
            else startRow = sheet.getLastRowNum();
        }
        //System.out.println("startRow is " + startRow);

        for (List<Object> rowData: data)
        {
            Row row = sheet.getRow(startRow);
            if (row == null) row = sheet.createRow(startRow);
            startRow++;  //Ready for next row to add
            int cellnum = colLocation - 1;
            for (Object val: rowData)
            {
                Cell cell = row.createCell(cellnum++);
                if (val == null) {
                    cell.setBlank();
                } else if (val instanceof String) {
                    cell.setCellValue((String) val);
                } else if (val instanceof Integer) {
                    cell.setCellValue((Integer) val);
                } else if (val instanceof Long) {
                    cell.setCellValue(((Long)val).intValue());
                } else if (val instanceof Double) {
                    cell.setCellValue((Double) val);
                } else if (val instanceof Boolean) {
                    if (((Boolean)val).booleanValue()) cell.setCellValue(1);
                    else cell.setCellValue(0);
                } else if (val instanceof LocalDate) {
                    cell.setCellValue((LocalDate) val);
                    cell.setCellStyle(this.dateCellStyle);
                } else if (val instanceof LocalDateTime) {
                    cell.setCellValue((LocalDateTime) val);
                    cell.setCellStyle(this.dateTimeCellStyle);
                } else {
                    cell.setCellValue(val.toString());
                }
            }
        }
        return data.size();
    }

    public void saveToFile(String fileName)
    {
        try
        {
            FileOutputStream out = new FileOutputStream(new File(fileName));
            this.workbook.write(out);
            out.close();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public void close()
    {
        try
        {
            this.workbook.close();
        }
        catch (IOException e)
        {
            //Don't really care. Do nothing.
        }
    }

    private List<String> rowToList(Row row)
    {
        Cell cell;
        int lastCellNum;
        List<String> csvLine = new ArrayList<>();

        // Check to ensure that a row was recovered from the sheet as it is
        // possible that one or more rows between other populated rows could be
        // missing - blank. If the row does contain cells then...
        if(row != null)
        {
            // Get the index for the right most cell on the row and then
            // step along the row from left to right recovering the contents
            // of each cell, converting that into a formatted String and
            // then storing the String into the csvLine ArrayList.
            lastCellNum = row.getLastCellNum();
            for(int i = 0; i <= lastCellNum; i++)
            {
                cell = row.getCell(i);
                if(cell == null)
                {
                    csvLine.add("");
                }
                else if(cell.getCellType() == CellType.ERROR)
                {
                    csvLine.add("");
                }
                else
                {
                    if(cell.getCellType() != CellType.FORMULA)
                    {
                        if (this.formattedCells)
                        {
                            csvLine.add(this.formatter.formatCellValue(cell));
                        }
                        else
                        {
                            if (cell.getCellType() == CellType.NUMERIC)
                            {
                                csvLine.add(cell.getNumericCellValue() + "");
                            }
                            else if (cell.getCellType() == CellType.BOOLEAN)
                            {
                                csvLine.add(cell.getBooleanCellValue() + "");
                            }
                            else
                            {
                                csvLine.add(cell.getStringCellValue());
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            csvLine.add(this.formatter.formatCellValue(cell, this.evaluator));
                        }
                        catch (Exception ex)
                        {
                            csvLine.add("#POI_EVAL_ERROR");
                        }
                    }
                }
            }
        }
        return csvLine;
    }

    private String csvDataToString(List<List<String>> csvData, int maxRowWidth)
    {
        StringBuilder buffer = new StringBuilder();
        List<String> line;
        String csvLineElement;

        for(int i = 0; i < csvData.size(); i++)
        {
            line = csvData.get(i);
            for(int j = 0; j < maxRowWidth; j++)
            {
                if(line.size() > j)
                {
                    csvLineElement = line.get(j);
                    if(csvLineElement != null)
                    {
                        buffer.append(escapeEmbeddedCharacters(csvLineElement));
                    }
                }
                if(j < (maxRowWidth - 1))
                {
                    buffer.append(separator);
                }
            }
            // Condition the inclusion of new line characters so as to
            // avoid an additional, superfluous, new line at the end of the file.
            if(i < (csvData.size() - 1))
            {
                buffer.append('\n');
            }
        }
        return buffer.toString().trim();
    }

    /**
     * Checks to see whether the field - which consists of the formatted
     * contents of an Excel worksheet cell encapsulated within a String - contains
     * any embedded characters that must be escaped. The method is able to
     * comply with either Excel's or UNIX formatting conventions in the
     * following manner;
     *
     * With regard to UNIX conventions, if the field contains any embedded
     * field separator or EOL characters they will each be escaped by prefixing
     * a leading backspace character. These are the only changes that have yet
     * emerged following some research as being required.
     *
     * Excel has other embedded character escaping requirements, some that emerged
     * from empirical testing, other through research. Firstly, with regards to
     * any embedded speech marks ("), each occurrence should be escaped with
     * another speech mark and the whole field then surrounded with speech marks.
     * Thus if a field holds <em>"Hello" he said</em> then it should be modified
     * to appear as <em>"""Hello"" he said"</em>. Furthermore, if the field
     * contains either embedded separator or EOL characters, it should also
     * be surrounded with speech marks. As a result <em>1,400</em> would become
     * <em>"1,400"</em> assuming that the comma is the required field separator.
     * This has one consequence in, if a field contains embedded speech marks
     * and embedded separator characters, checks for both are not required as the
     * additional set of speech marks that should be placed around ay field
     * containing embedded speech marks will also account for the embedded
     * separator.
     *
     * It is worth making one further note with regard to embedded EOL
     * characters. If the data in a worksheet is exported as a CSV file using
     * Excel itself, then the field will be surounded with speech marks. If the
     * resulting CSV file is then re-imports into another worksheet, the EOL
     * character will result in the original simgle field occupying more than
     * one cell. This same 'feature' is replicated in this classes behaviour.
     *
     * @param field An instance of the String class encapsulating the formatted
     *        contents of a cell on an Excel worksheet.
     * @return A String that encapsulates the formatted contents of that
     *         Excel worksheet cell but with any embedded separator, EOL or
     *         speech mark characters correctly escaped.
     */
    private String escapeEmbeddedCharacters(String field) {
        StringBuilder buffer;

        // If the fields contents should be formatted to confrom with Excel's
        // convention....
        if(this.escapeStyle == POIReader.EXCEL_STYLE_ESCAPING) {

            // Firstly, check if there are any speech marks (") in the field;
            // each occurrence must be escaped with another set of spech marks
            // and then the entire field should be enclosed within another
            // set of speech marks. Thus, "Yes" he said would become
            // """Yes"" he said"
            if(field.contains("\"")) {
                buffer = new StringBuilder(field.replaceAll("\"", "\\\"\\\""));
                buffer.insert(0, "\"");
                buffer.append("\"");
            }
            else {
                // If the field contains either embedded separator or EOL
                // characters, then escape the whole field by surrounding it
                // with speech marks.
                buffer = new StringBuilder(field);
                if((buffer.indexOf(this.separator)) > -1 ||
                        (buffer.indexOf("\n")) > -1) {
                    buffer.insert(0, "\"");
                    buffer.append("\"");
                }
            }
            return(buffer.toString().trim());
        }
        // The only other formatting convention this class obeys is the UNIX one
        // where any occurrence of the field separator or EOL character will
        // be escaped by preceding it with a backslash.
        else {
            if(field.contains(this.separator)) {
                field = field.replaceAll(this.separator, ("\\\\" + this.separator));
            }
            if(field.contains("\n")) {
                field = field.replaceAll("\n", "\\\\\n");
            }
            return(field);
        }
    }

    public static Calendar getJavaCalendar(double d)
    {
        return DateUtil.getJavaCalendar(d);
    }

    public static void main(String[] args)
    {
        String filename = args[0];
        String sheetName = args[1];
        System.out.println("File name is " + filename + ", Sheet name is " + sheetName);

        POIReader poi = null;
        try
        {
            poi = new POIReader(filename, DEFAULT_SEPARATOR, EXCEL_STYLE_ESCAPING, false);
            String[] sheetNames = poi.getSheetNames();
            for (String name : sheetNames)
            {
                System.out.println("Sheet name is " + name);
            }

            String csv = poi.getSheetNameAsCsv(sheetName);
            //System.out.println("Sheet " + sheetName + " CSV content size is " + csv.length());
            int chunkLen = Math.min(2000, csv.length() - 1);
            System.out.println("Sheet " + sheetName + " first part is " + csv.substring(0, chunkLen));

            //csv = poi.getSheetIdAsCsv(0);
            //System.out.println("Sheet " + 0 + " CSV content size is " + csv.length());

            /*
            List<Object> row0 = new ArrayList<>();
            row0.add("ID");
            row0.add("Name");
            row0.add("DOB");
            row0.add("TS");
            row0.add("Years");
            row0.add("Employed?");

            List<Object> row1 = new ArrayList<>();
            row1.add(1);
            row1.add("John");
            row1.add(LocalDate.parse("2010-10-21"));
            row1.add(LocalDateTime.parse("2005-12-23T09:56:13"));
            row1.add(4.37);
            row1.add(true);

            List<Object> row2 = new ArrayList<>();
            row2.add(2);
            row2.add("Joe");
            row2.add(LocalDate.parse("2011-11-21"));
            row2.add(LocalDateTime.parse("2006-02-23T11:26:11"));
            row2.add(2.456);
            row2.add(false);

            List<List<Object>> rows = new ArrayList<>();
            rows.add(row0);
            rows.add(row1);
            rows.add(row2);

            poi.writeSheetData(sheetName, rows, true, -1, -1);
            poi.saveToFile(filename);
             */
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
