*****************************************************
** title:      poiTools
** author:     John MacEnri
** created:    Mar 2020
** copyright:  Copyright (c) 2020, Crowley Carbon Ltd
** licence:    MIT
*****************************************************

Overview [#overview]
********************
The poiTools Extension is a thin Fantom wrapper around the [Apache POI]`https://poi.apache.org/` java library.
It exposes a small number of Axon functions for constructing and using a PoiReader instance.
The extension allows the Axon developer to directly access MS Excel files of almost any version
and read a single sheet in as a Str that can be further parsed by the calling Axon (e.g. ioReadCsv)

Note: Versions from 1.1.0+ are built for SkySpark 3.1.1+

See the Axon function docs for details on how to use the extension.
  - `poiReaderOpen`
  - `poiReaderOpenBuf`
  - `poiListSheets`
  - `poiReadSheetByNameAsStr`
  - `poiReadSheetByIndexAsStr`
  - `poiReaderClose`
  - `poiReaderNew`
  - `poiWriteSheetData`
  - `poiSaveToFile`


