/**
=====================
The MIT License (MIT)
=====================

Copyright (c) 2020 Crowley Carbon Ltd

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// History:
//  Mar 2020 John MacEnri Creation
//

using [java] com.crowleycarbon.poi::POIReader
using [java] java.nio::ByteBuffer
using [java] fanx.interop::Interop
using [java] fanx.interop::ByteArray
using [java] java.util::Calendar as JCal
using [java] java.util::List
using [java] java.util::ArrayList
using [java] java.time::LocalDate
using [java] java.time::LocalDateTime
using haystack
using axon
using skyarcd

const class PoiToolsLib
{

  **
  ** Construct an empty new POIReader. Useful when writing data to a sheet and then saving to new file.
  **
  ** Examples:
  **   poiReaderNew()
  **   poiReaderNew({separator:",", formattedCells:true})
  **
  ** Side effects:
  **   - Returns an instance of POIReader which must be passed in on all subsequent calls that wish to use this reader
  **
  @Axon static POIReader poiReaderNew(Dict opts := Etc.emptyDict)
  {
    Str? separator := ","
    Int? escapeStyle := 0
    Bool? formattedCells := false
    if (opts["separator"] != null) separator = opts->separator as Str
    if (opts["escapeStyle"] != null) escapeStyle = opts->escapeStyle as Int
    if (opts["formattedCells"] != null) formattedCells = opts->formattedCells as Bool
    return POIReader(separator, escapeStyle, formattedCells)
  }

  **
  ** Construct a POIReader from the provided file name
  ** When called from Axon, the filename must start with "io/", so assume that and prefix with project absolute path
  **
  ** Examples:
  **   poiReaderOpen("myFile.xlsx")
  **   poiReaderOpen("myFile.xlsx", {separator:","})
  **
  ** Side effects:
  **   - Returns an instance of POIReader which must be passed in on all subsequent calls that wish to use this reader
  **
  @Axon static POIReader poiReaderOpen(Str filename, Dict opts := Etc.emptyDict)
  {
    Str? separator := ","
    Int? escapeStyle := 0
    Bool? formattedCells := false
    if (opts["separator"] != null) separator = opts->separator as Str
    if (opts["escapeStyle"] != null) escapeStyle = opts->escapeStyle as Int
    if (opts["formattedCells"] != null) formattedCells = opts->formattedCells as Bool

    if (filename.startsWith("/proj")) filename = Context.cur.sys.varDir.pathStr + filename
    else filename = Context.cur.proj.dir.pathStr + filename

    return POIReader(filename, separator, escapeStyle, formattedCells)
  }

  **
  ** Construct a POIReader for the provided binary byte buffer. The Buf instance should be the binary contents of
  ** any of the support Excel file versions.
  **
  ** Examples:
  **   poiReaderOpen(smbReaderOpen("10.0.0.1").smbReaderConnect("shareName").smbReaderReadBinaryFile("dirName", "myFile.xlsx"))
  **
  ** Side effects:
  **   - Returns an instance of POIReader which must be passed in on all subsequent calls that wish to use this reader
  **
  @Axon static POIReader poiReaderOpenBuf(Buf buf, Dict opts := Etc.emptyDict)
  {
    Str? separator := ","
    Int? escapeStyle := 0
    Bool? formattedCells := false
    if (opts["separator"] != null) separator = opts->separator as Str
    if (opts["escapeStyle"] != null) escapeStyle = opts->escapeStyle as Int
    if (opts["formattedCells"] != null) formattedCells = opts->formattedCells as Bool
    return POIReader(Interop.toJava(buf.trim).array, separator, escapeStyle, formattedCells)
  }

  **
  ** List all the sheet names found in the Excel document represented by the provided POIReader instance.
  **
  ** Examples:
  **   poiReaderOpen("myFile.xlsx").poiListSheets
  **
  ** Side effects:
  **   - Returns the array of sheet names found
  **
  @Axon static Str[] poiListSheets(POIReader poi)
  {
     return poi.getSheetNames()
  }

  **
  ** Read the content of a specific sheet, identified by name, returned as a Str which can be further parsed by the calling Axon function.
  **
  ** Examples:
  **   poiReaderOpen("myFile.xlsx").poiReadSheetByNameAsStr("Sheet1")
  **
  ** Side effects:
  **   - Returns the contents of the named sheet as a Str, suitable for parsing by ioReadCsv
  **
  @Axon static Str poiReadSheetByNameAsStr(POIReader poi, Str sheetname)
  {
    return poi.getSheetNameAsCsv(sheetname)
  }

  **
  ** Read the content of a specific sheet, identified by index, returned as a Str which can be further parsed by the calling Axon function.
  **
  ** Examples:
  **   poiReaderOpen("myFile.xlsx").poiReadSheetByIndexAsStr(0)
  **
  ** Side effects:
  **   - Returns the contents of the sheet at a specific index as a Str, suitable for parsing by ioReadCsv
  **
  @Axon static Str poiReadSheetByIndexAsStr(POIReader poi, Number sheetIndex)
  {
    return poi.getSheetIdAsCsv(sheetIndex.toInt)
  }

  @Axon static POIReader poiSaveToFile(POIReader poi, Str filename)
  {
    if (filename.startsWith("/proj")) filename = Context.cur.sys.varDir.pathStr + filename
    else filename = Context.cur.proj.dir.pathStr + filename
    poi.saveToFile(filename)
    return poi
  }

  **
  ** Write the Grid of data to a specific sheet, identified by name.
  **
  ** Examples:
  **   poiReaderOpen("test.xlsx")
  **     .poiWriteSheetData(
  **        "Sheet1",
  **        [{name:"n1", age:12},{name:"n2", age:10}].toGrid,
  **        {
  **          writeColHeaders:true, clearSheet:false, rowLocation:1, colLocation:1,
  **          dateFormat:"YYYY-MM-DD", dateTimeFormat:"YYYY-MM-DD hh:mm:ss"
  **        })
  **     .poiSaveToFile("test.xlsx")
  **     .poiReaderClose
  **
  ** If the sheet of the specified name doesn't exist, it will be created.
  **
  ** Options work as follows:
  ** - 'rowLocation' is 1 based. Default is 0. Any value < 1 indicates append the data after existing rows.
  ** - 'colLocation' is 1 based. Default is 1.
  ** - 'clearSheet' when true will cause all rows after 'rowLocation' to be cleared. When false, only the rows where the data lands will be replaced.
  ** - 'dateFormat' defaults to "YYYY-MM-DD", so any cell in the Grid that is of type date, will be formatted correctly in Excel
  ** - 'dateTimeFormat' defaults to "YYYY-MM-DD hh:mm:ss", so any cell in the Grid that is of type dateTime, will be formatted correctly in Excel
  **
  ** Side effects:
  **   - Updates the contents of the provided POIReader and returns it, ready for writing to file.
  **
  @Axon static POIReader poiWriteSheetData(POIReader poi, Str sheetName, Grid data, Dict? opts := null)
  {
       writeColHeaders := true
       clearSheet := false  // When true, clear all row locations >= rowLocation. So for rowLocation = 1 all rows will be removed.
                            // When false, no rows are cleared, and just the rows where the incoming data is written to, will be overwritten.
       rowLocation := 0    // 1 based row index to write data at. 0 indicates append to existing rows.
       colLocation := 1    // 1 based column index to write data at.

       if (opts != null)
       {
            if (opts.get("writeColHeaders") != null) writeColHeaders = opts.get("writeColHeaders")
            if (opts.get("clearSheet") != null) clearSheet = opts.get("clearSheet")
            if (opts.get("rowLocation") != null) rowLocation = (opts.get("rowLocation")  as Number).toInt
            if (opts.get("colLocation") != null) colLocation = (opts.get("colLocation")  as Number).toInt

            if (opts.get("dateFormat") != null) poi.setDateTimeFormat(opts.get("dateFormat"))
            if (opts.get("dateTimeFormat") != null) poi.setDateTimeFormat(opts.get("dateTimeFormat"))
       }

       List rows := ArrayList()
       if (writeColHeaders)
       {
            List headerRow := ArrayList()
            data.colNames.each |cn|
            {
                 headerRow.add(cn)
            }
            rows.add(headerRow)
       }

       data.each |row|
       {
            List newRow := ArrayList()
            row.each |Obj? val|
            {
                 if (val != null)
                 {
                    if (val.typeof == Str#)
                    {
                        newRow.add(val)
                    }
                    else if (val.typeof == Number#)
                    {
                        n := val as Number
                        if (n.isInt) newRow.add(n.toInt)
                        else newRow.add(n.toFloat)
                    }
                    else if (val.typeof == Bool#)
                    {
                        b := val as Bool
                        newRow.add(b)
                    }
                    else if (val.typeof == Date#)
                    {
                        d := val as Date
                        m := Month.vals.index(d.month) + 1
                        ld := LocalDate.of(d.year,m,d.day)
                        newRow.add(ld)
                    }
                    else if (val.typeof == DateTime#)
                    {
                        dt := val as DateTime
                        m := Month.vals.index(dt.month) + 1
                        ldt := LocalDateTime.of(dt.year,m,dt.day,dt.hour,dt.min,dt.sec)
                        newRow.add(ldt)
                    }
                 }
                 else newRow.add(null)
            }
            rows.add(newRow)
       }

       poi.writeSheetData(sheetName, rows, clearSheet, rowLocation, colLocation)
       return poi
  }

  **
  ** Close the provided POIReader instance.
  **
  ** Examples:
  **   poiReaderOpen("myFile.xlsx").poiReaderClose
  **
  ** Side effects:
  **   - Closes completely the provided POIReader instance.
  **
  @Axon static Void poiReaderClose(POIReader poi)
  {
    poi.close
  }

  @Axon static Obj? getDateTimeFromExcelDouble(Number excelSerialDate, Str tz := "UTC", Bool checked := true)
  {
    try
    {
        f := excelSerialDate.toFloat
        JCal? jc := POIReader.getJavaCalendar(f)
        if (f <= 1.0f)
        {
            return Time(jc.get(JCal.HOUR_OF_DAY),jc.get(JCal.MINUTE),jc.get(JCal.SECOND))
        }
        else
        {
            return DateTime(jc.get(JCal.YEAR),
                            Month.vals[jc.get(JCal.MONTH)],
                            jc.get(JCal.DAY_OF_MONTH),
                            jc.get(JCal.HOUR_OF_DAY),
                            jc.get(JCal.MINUTE),
                            jc.get(JCal.SECOND),
                            0,
                            TimeZone.fromStr(tz))
        }
    }
    catch (Err ex)
    {
        if (checked) throw(ex)
        else return null
    }
  }
}

